window.addEventListener('load', (event) => {
  const measurements = []
  var average
  const geolocation = navigator.geolocation
  var measurementIntervalId

  function main() {
    const stopForm = document.getElementById("stop")
    stopForm.addEventListener("submit", event => {
      event.preventDefault()

      stopMeasuring()
    })

    const startForm = document.getElementById("start")
    startForm.addEventListener("submit", event => {
      event.preventDefault()

      startMeasuring()
    })
  }

  function startMeasuring() {
    if (measurementIntervalId !== undefined) {
      return
    }
    measurementIntervalId = window.setInterval(takeMeasurement, 1000)
  }
  function stopMeasuring() {
    window.clearInterval(measurementIntervalId)
    measurementIntervalId = undefined
  }

  function takeMeasurement() {
    geolocation.getCurrentPosition((position) => {
      const measurement = {
        position: position,
      }
      measurements.push(measurement)

      showMeasurement(measurement)
      updateAverage()
    },
    (error) => {
      console.log(error.message)
      if (error.code != GeolocationPositionError.TIMEOUT) {
        stopMeasuring()
      }
    },
    {
      maximumAge: 0,
      timeout: 900,
      enableHighAccuracy: true,
    }
  )
  }

  function updateAverage() {
    const totalMeasurements = measurements.length
    var latitudeSum = 0
    var longitudeSum = 0
    for (const measurement of measurements) {
      coords = measurement.position.coords
      latitudeSum += coords.latitude
      longitudeSum += coords.longitude
    }
    average = {
      latitude: latitudeSum / totalMeasurements,
      longitude: longitudeSum / totalMeasurements,
    }

    showAverage()
  }

  function showMeasurement(measurement) {
    const li = document.createElement("li")
    const p = measurement.position.coords
    li.textContent = `${p.latitude} ${p.longitude}`
    document.getElementById('measurements').appendChild(li)
  }

  function showAverage() {
    const latitudeElement = document.getElementById("average-latitude")
    latitudeElement.textContent = average.latitude
    const longitudeElement = document.getElementById("average-longitude")
    longitudeElement.textContent = average.longitude
  }

  main()
})